
#import
import rasterio
import shutil
from rasterio.warp import calculate_default_transform,reproject

def Reprojection(src_path,dst_crs,dst_path,dst_width,dst_height):
    """
    src_path : (source path) path to the raster you want to reproject (the file must be readable by rasterio)
    dst_crs : (destination coordinate reference system) under the form 'ESPG:XXXX' with a 4 digit code
    dst_path : (destination path) where you want to save your reprojected raster
    dst_width : (destination width) width (in pixel) of the reprojected raster
    dst_height : (destination height) height (in pixel) of the reprojected raster

    This function reprojects your raster in the crs of your choice
    """

    src_img=rasterio.open(src_path)

    # if the source image is already in the destination crs
    if src_img.crs == dst_crs:
        return ('The source image is already in the destination crs !')

    # else we reproject
    else:
        with rasterio.open(src_path) as src:
            transform, width, height = calculate_default_transform(src.crs, dst_crs, src.width, src.height, *src.bounds, dst_width=dst_width, dst_height=dst_height)
            kwargs = src.meta.copy()
            kwargs.update({
                'crs': dst_crs,
                'transform': transform,
                'width': width,
                'height': height})

            shutil.copy(src_path,dst_path)

            with rasterio.open(dst_path, 'w', **kwargs) as dst:
                for i in range(1, src.count + 1):
                    reproject(
                        source=rasterio.band(src, i),
                        destination=rasterio.band(dst, i),
                        src_transform=src.transform,
                        src_crs=src.crs,
                        dst_transform=transform,
                        dst_crs=dst_crs)


