import numpy as np
import tifffile
import pandas as pd
import whittaker

"""Ce fichier obtient le fichier de données total (réflectance pour chaque bande + indices pour le moment)
pour chaque pixel issu fichier ground truth ('gt\dfpixel.csv')"""

#Opening hyperspectral files

nom_images = ["1", "1b", "2", "3", "3b", "4", "Premol"]
hi_tiff = []
hi_arr = []
hi_metadata = []

for nom in nom_images:
    hi_tiff.append(tifffile.TiffFile(r"hi\{}.tif".format(nom)))
    
for image in hi_tiff:
    hi_arr.append(image.asarray())
    hi_metadata.append(image.shaped_metadata[0])
    
hi_arr = np.array(hi_arr)
hi_metadata = np.array(hi_metadata)

###Smoothing reflectance spectra
print("Smoothing pictures ...")
count = 0
for image in hi_arr:
    print("{}/{}".format(count, len(hi_arr)))
    for r in range(image.shape[0]):
        for c in range(image.shape[1]):
            image[r,c] = whittaker.whittaker_smooth(image[r,c], 12)
    count += 1
print("Smoothing done.")
    

###Opening ground truth

df = pd.read_csv(r"gt\df_pixel.csv")


### Adding reflectances for each band

print("Adding reflectances ...")
liste_bande_i = []

nb_bandes = len(hi_metadata[0]["wavelength"])

i = 1

for bande in range(nb_bandes): 
    print("{}%".format(bande/nb_bandes*100))
    for index, row in df.iterrows():

        r = row["row"]
        c = row["col"]
        
        plotid = str(row["plotid"])
        
        num_image = nom_images.index(plotid) #to be improved
        
        liste_bande_i.append(hi_arr[num_image][r,c][bande])
        
    df['Bande' + str(i)] = np.array(liste_bande_i)
    
    liste_bande_i = []
    
    i+=1

print("Adding reflectances : done.")

print("Adding vegetation index ...")

df['RVI'] = df["Bande107"]/df["Bande73"]
df['NIRR'] = df["Bande99"]-df["Bande69"]
df['NDVI'] = (df['Bande114']-df['Bande70'])/(df['Bande114']+df['Bande70'])
df['GRR'] = (df['Bande41']-df['Bande69'])/(df['Bande41']+df['Bande69'])
df['DD'] = (2*df['Bande147'] - df['Bande93']) - (df['Bande65']-df['Bande37'])
df['ARVI'] = (df['Bande124']-2*df['Bande68']-df['Bande14'])/(df['Bande124']+2*df['Bande68']+df['Bande14'])
df['GARI'] = (df['Bande92']-(df['Bande36']-(df['Bande14']-df['Bande36'])))/(df['Bande92']+(df['Bande36']-(df['Bande14']-df['Bande36'])))
df['GNDVI'] = (df['Bande93']-df['Bande37'])/(df['Bande93']+df['Bande37'])
#df['VARI'] = (df['Bande41']-['Bande69'])/(df['Bande41']+['Bande69']+df['Bande21'])
df['EVI1'] = 2.5*((df['Bande125']-df['Bande65'])/(df['Bande125']+ 6*df['Bande65'] -7.5* df['Bande15'] + 1))
df['EVI2'] = 2.5*((df['Bande125']-df['Bande65'])/(df['Bande65']*2.4+df['Bande125']+1))
df['PRI'] = (df['Bande33']-df['Bande43'])/(df['Bande33']+df['Bande43'])
df['RENDVI'] = (df['Bande93']-df['Bande81'])/(df['Bande93']+df['Bande81'])
df['mSRI'] = (df['Bande93']-df['Bande9'])/(df['Bande81']-df['Bande9'])
df['mND'] = (df['Bande93']-df['Bande81'])/(df['Bande93']+df['Bande81']-2*df['Bande9'])
df['GR'] = df['Bande37']/df['Bande69']
df['BR'] = (df['Bande69']/df['Bande19']) * (df['Bande37']/df['Bande19']) * (df['Bande85']/df['Bande19']) * (df['Bande115']/df['Bande19'])
df['RR'] = (df['Bande114']/df['Bande68']) * (df['Bande36']/df['Bande68']) * (df['Bande114']/df['Bande84'])
df['IPVI'] = df['Bande106']/(df['Bande72']+df['Bande106'])
df['NDRE'] = (df['Bande102']-df['Bande84'])/(df['Bande102']+df['Bande84'])
df['PSRI'] = (df['Bande72']-df['Bande24'])/df['Bande92']
    
print("Adding vegetation index : done.") 

 


###Changing species name
#To each specie a number 0-13 is associated


mapping = {'ABAL': 0, 'ACPS': 1, 'BEPE' : 2, 'BEsp' : 3, 'COAV' : 4, 'FASY' : 5, 'FREX' : 6, 'PIAB' : 7, 'PICE' : 8, 'PIUN' : 9, 'POTR' : 10, 'SOAR' : 11, 'SOAU' : 12}
df = df.applymap(lambda s: mapping.get(s) if s in mapping else s)

print("Adding LiDAR data ...")

#Add lidar data

lidar1 = pd.read_csv(r"lidar_features\raster\img_1.csv")
lidar1b = pd.read_csv(r"lidar_features\raster\img_1b.csv")
lidar2= pd.read_csv(r"lidar_features\raster\img_2.csv")
lidar3 = pd.read_csv(r"lidar_features\raster\img_3.csv")
lidar3b = pd.read_csv(r"lidar_features\raster\img_3b.csv")
lidar4= pd.read_csv(r"lidar_features\raster\img_4.csv")
lidarPremol = pd.read_csv(r"lidar_features\raster\img_Premol.csv")

frames = [lidar1, lidar1b, lidar2, lidar3, lidar3b, lidar4, lidarPremol]

df_lidar = pd.concat(frames)
df_lidar = df_lidar.rename(columns={'plot': 'plotid','0':'row', '1':'col'})





#make sure both "plotid" columns are of the same type for merging
df_lidar["plotid"] = df_lidar["plotid"].astype(str) 
df["plotid"] = df["plotid"].astype(str) 


merged = df.merge(df_lidar, on=['plotid', 'row', 'col']) #works fine !
merged = merged.drop([12853]) #unexplained issue : specie = 'ND'. Works fine without that row

print("Adding LiDAR data: done.")

#creating the final CSV file
merged.to_csv('data_train_full.csv', index=False)

print("Successfully created the CSV file.")




   

    









