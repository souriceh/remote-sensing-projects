import numpy as np
import cv2
import os
from skimage import data
import matplotlib.pyplot as plt

"""This function records each frame of the video"""
def image_recovery (name_video, nom_fichier_image) :
    #Opening of the video
    liste_video=[]
    cam=cv2.VideoCapture(name_video)
    #Creation of the file location if it does not exist
    try : 
        if not os.path.exists(nom_fichier_image):
            os.makedirs(nom_fichier_image)
    except OSError : 
        print('Error : Creating directory of data')
                
    currentframe = 0
    while(True):
        #reading from frame
        ret,frame=cam.read()
        if ret : 
            #if video is still left continue creating images
            name='./'+nom_fichier_image+'/frame'+str(currentframe)+'.jpg'
            print('Creating ...'+name)
            #writing the extracted images
            cv2.imwrite(name,frame)
            currentframe += 1
        else:
            break
        
    cam.release()
    cv2.destroyAllWindows()