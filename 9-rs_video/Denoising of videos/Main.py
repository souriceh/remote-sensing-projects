import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
from Image_recovery1 import image_recovery
from Denoising_video2 import denoising_video_multi
from Denoising_video2 import denoising_video_single


"""File name definition"""
name_video = 'Beach3_5Hz.avi'
nom_fichier_image = 'test123' #Choose the name of the file that will contain the orinals frames
file_image_denoising = 'test234' #Choose the name of the file that will contain the denoised frames

"""Function call"""
#This function records the frames of a video
image_recovery (name_video, nom_fichier_image)

#What method do you want to use  : 1 = fastNlMeansDenoisingColoredMulti ; 2 = fastNlMeansDenoisingColored
choose = 2

if choose == 1 : 
    #This function denoised and records the denoised frames a video with the function
    #fastNlMeansDenoisingColoredMulti()
    denoising_video_multi (name_video, file_image_denoising)
    
else :
    #This function denoised and records the denoised frames a video with the function
    #fastNlMeansDenoisingColored()
    denoising_video_single (name_video, file_image_denoising,nom_fichier_image)




