You have to download the files and run them on spyder or another python IDE because we couldn't get it to work on Jupyter.

These 3 Python files will allow you to denoise videos.
You just have to put them in the same folder. 
You go to Main.py and you choose the necessary file names, you choose the method to use and you launch the program

Main.py need the function contained in denoising_video2 and Image_recovery1 to work.

Image_recovery1.py contains a function that separates the different frames of the video and saves them in a file.

Denoising_video2.py contains 3 functions :
The first one counts the number of frame in a video
The second one denoises the video using fastNlMeansDenoisingColoredMulti and saves the different frames in a file.
The third one denoises the video using fastNlMeansDenoisingColored and saves the different frames in a file.