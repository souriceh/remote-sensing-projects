import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import os 

"""This function counts the frames in a video"""
def number_of_frame (name_video) :
    cap = cv.VideoCapture(name_video)
    numb_frame = 0
    while True:
        ret = cap.read()[0]
        if ret:
            numb_frame = numb_frame + 1
            print(numb_frame)
        else:
            return numb_frame



"""This function denoise the frames 1 by 1"""
"""With fastNlMeansDenoisingColoredMulti"""
def denoising_video_multi (name_video, file_image_denoising):  
    #Creation of the file location if it does not exist
    try : 
        if not os.path.exists(file_image_denoising):
            os.makedirs(file_image_denoising)
    except OSError : 
        print('Error : Creating directory of data')
    
    numb_frame = number_of_frame(name_video)
    cap = cv.VideoCapture(name_video)

    # create a list of frames
    img = [cap.read()[1] for i in range(numb_frame)] 
    # convert all to a good format
    colored = [cv.cvtColor(i, cv.COLOR_BGR2RGB) for i in img]
    # convert all to float64
    colored = [np.float64(i) for i in colored]
    
    colored = [np.uint8(np.clip(i,0,255)) for i in colored]
    
    frame = 2
    while frame < numb_frame - 2 :
        #denoising function. You can find the documentation of this function on the website of opencv
        dst = cv.fastNlMeansDenoisingColoredMulti(colored, frame, 5, None, 3, 3, 7, 21)
        #we have to modify the color format because we modified it above for the function
        correct_color = cv.cvtColor(dst, cv.COLOR_RGB2BGR)
        name_frame = './'+file_image_denoising+'/frame'+str(frame)+'.jpg'
        cv.imwrite(name_frame,correct_color)
        frame = frame+1
        print("Frame = ",frame)



"""This function denoise the frames 1 by 1"""
"""With fastNlMeansDenoisingColored"""
def denoising_video_single (name_video, file_image_denoising,nom_fichier_image):
    #Creation of the file location if it does not exist
    try : 
        if not os.path.exists(file_image_denoising):
            os.makedirs(file_image_denoising)
    except OSError : 
        print('Error : Creating directory of data')
        
    numb_frame = number_of_frame(name_video)
        
    for i in range (0,numb_frame):
        #Reading the image to denoised from the file with the original frames
        img = cv.imread('./'+nom_fichier_image+'/frame'+str(i)+'.jpg')

        #denoising function. You can find the documentation of this function of the website of opencv
        dst = cv.fastNlMeansDenoisingColored(img,None,3,3,7,21)
        #writing of the denoised image
        cv.imwrite('./'+file_image_denoising+'/frame'+str(i)+'.jpg',dst)