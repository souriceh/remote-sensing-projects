You must download the file to use them, you must put them in the same folder as the video. 
These files can't be used on Jupyter, you have to use an editor like Spyder.

You must also install the tracker and contour modules with the "!pip install tracker" and "!pip install contours" formulas in the console.