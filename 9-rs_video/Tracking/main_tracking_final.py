# -*- coding: utf-8 -*-
"""
Created on Thu May 27 08:33:26 2021

@author: vince
"""
import cv2
import matplotlib.pyplot as plt 
from tracker import *
from contours import *
from PIL import Image 
import numpy as np
import sys

#modifiable parameters (it then remains to define the roi, line 31)
varThreshold=45
contour_area=50

#create tracker object 
tracker = EuclideanDistTracker()

cap= cv2.VideoCapture("videos\Beach1_5Hz.avi")
# object detection with a stable camera
object_detector = cv2.createBackgroundSubtractorMOG2(history=100,varThreshold=varThreshold) #the function cv2.createBackgroundSubtractorMOG2 puts in black what is fixed and in white what moves 

# launch of the video (the video is broken down into images )
i=0
while(True):
    ret, frame = cap.read()

    if (cap.grab()==False):
        break
    
    
    #extraction of a part of the image 
    roi= frame[:,:] # roi to Region Of Interest
   
    
    # Object detection
    mask= object_detector.apply(roi) 
#    mask_1=object_detector.apply(frame) 
    
    _,mask = cv2.threshold(mask,254,255,cv2.THRESH_BINARY) #Avoid shadows to be considered as moving objects
    
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE ) #the ret_tree and chain.. functions define how the information is extracted, and this function allow to find contours of the moving objects
    detections=[]
    
    #Construction of rectangles around moving objects 
    for cnt in contours:
        #calculate the area of the contours and remove the small elements 
        area= cv2.contourArea(cnt) # is the area of the contours
        
        if area >contour_area:

            x,y,w,h= cv2.boundingRect(cnt) #this fonction allow to obtain information to draw a rectangle enclosing a shape 
            cv2.rectangle(roi, (x,y), (x+w, y+h),(0,255,0),2)#constructs a rectangle on the roi, with the opposite corners specified by the parameter coordinates 2 and 3
            
            detections.append([x,y,w,h]) # Detection contains the information on the rectangles at all times
        
    #identification of tracked objects by id 
    boxes_ids= tracker.update(detections)#in boxes_ids there are the coordinates x , y , w and h + the corresponding id)
 
    
    for box_id in boxes_ids:
        x,y,w,h,id= box_id
        
        cv2.putText(roi,str(id),(x,y-15), cv2.FONT_HERSHEY_PLAIN, 2, (255,0,0),2)#to add text, parameter 1: concerned area 2:text to write, 3:font, 4:size, 5:colour, 6:thickness) 
    #display of 3 windows, the original video,the Roi, and the Roi processed to display moving objects
    cv2.imshow("Frame",frame) 
    cv2.imshow("mask",mask)
    cv2.imshow("Roi",roi)
#    cv2.imshow("mask_1",mask_1)
    cv2.waitKey(    0) #waitKey let some times between each frames,to see the video frame by frame, just write cv2.waitkey(0), and to pass from a frame to another press any key 
    
cap.release() #close the video file

cv2.destroyAllWindows()



  